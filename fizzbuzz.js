// Zenvia Test FizzBuzz
function fizzBuzz(num) {
	if (num % 15 === 0)	
		return 'FizzBuzz';
	else if (num % 5 === 0)
		return 'Buzz';
	else if (num % 3 === 0)
		return 'Fizz';
	
	return num;
}

function newFizzBuzz(num) {
	if (num % 15 !== 0) {
		if(num.toString().indexOf('5') > -1)
			return 'Buzz';
		else if (num.toString().indexOf('3') > -1)
			return 'Fizz';
	}
	
	return fizzBuzz(num);
}

function run(isStageTwo) {
	for (let i = 1; i <= 100; i++)
		console.log(isStageTwo === true ? newFizzBuzz(i) : fizzBuzz(i));
}

run();
run(true);