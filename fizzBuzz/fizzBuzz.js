/*
 * Zenvia Test - FizzBuzz
 * @Author: Felipe Fett
*/ 

/*
 * Toggle the stages for each new instance of FizzBuzz
*/	
if(!hasNewRules) var hasNewRules = true;

/*
 *	FizzBuzz constructor
*/
const FizzBuzz = function() {
	this.hasNewRules = hasNewRules = !hasNewRules;
};

/*
 *	Returns "Fizz", "Buzz", "FizzBuzz" or [num] for any integer input
*/
FizzBuzz.prototype.fizzBuzz = function (num) {
	/* 
	 *	Return "Fizz" for multiples of 3, "Buzz" for multiples of 5, 
	 *	"FizzBuzz" for multiples of both and the entry for the others.
	*/ 
	const stageOne = function(num) {
		if (num % 15 === 0)	
			return 'FizzBuzz';
		else if (num % 5 === 0)
			return 'Buzz';
		else if (num % 3 === 0)
			return 'Fizz';

		return num;
	};
	
	/* 
	 *	Aditional "Fizz" for inputs containg 3 and "Buzz" for inputs containg 5.
	*/
	const stageTwo = function(num) {
		if (num % 15 !== 0) {
			if(num.toString().indexOf('5') > -1)
				return 'Buzz';
			else if (num.toString().indexOf('3') > -1)
				return 'Fizz';
		} return stageOne(num);
	};
	
	return this.hasNewRules ? stageTwo(num) : stageOne(num);
};

/* 
 *	Runs fizzBuzz from 1 to 100.
*/
FizzBuzz.prototype.run = function () {
	for (let i = 1; i <= 100; i++)
		console.log(this.fizzBuzz(i));
};