describe('FizzBuzz Test [Default]', function() {
	let fb = new FizzBuzz(); // Default case
	it('Should return "1" when we are testing the number "1"', function() { expect(fb.fizzBuzz(1)).toEqual(1); });
	it('Should return "Fizz" when we are testing the number "3"', function() { expect(fb.fizzBuzz(3)).toEqual('Fizz'); });
	it('Should return "Buzz" when we are testing the number "5"', function() { expect(fb.fizzBuzz(5)).toEqual('Buzz'); });
	it('Should return "13" when we are testing the number "13"', function() { expect(fb.fizzBuzz(13)).toEqual(13); });
	it('Should return "FizzBuzz" when we are testing the number "15"', function() { expect(fb.fizzBuzz(15)).toEqual('FizzBuzz'); });
	it('Should return "26" when we are testing the number "26"', function() { expect(fb.fizzBuzz(26)).toEqual(26); });
	it('Should return "32" when we are testing the number "32"', function() { expect(fb.fizzBuzz(32)).toEqual(32); });
	it('Should return "52" when we are testing the number "52"', function() { expect(fb.fizzBuzz(52)).toEqual(52); });
});

describe('FizzBuzz Test [New Rules]', function() {
	let fb = new FizzBuzz(); // Add new rules for fizzBuzz function
	it('Should return "1" when we are testing the number "1"', function() { expect(fb.fizzBuzz(1)).toEqual(1); });
	it('Should return "Fizz" when we are testing the number "3"', function() { expect(fb.fizzBuzz(3)).toEqual('Fizz'); });
	it('Should return "Buzz" when we are testing the number "5"', function() { expect(fb.fizzBuzz(5)).toEqual('Buzz'); });
	it('Should return "Fizz" when we are testing the number "13"', function() { expect(fb.fizzBuzz(13)).toEqual('Fizz'); });
	it('Should return "FizzBuzz" when we are testing the number "15"', function() { expect(fb.fizzBuzz(15)).toEqual('FizzBuzz'); });
	it('Should return "26" when we are testing the number "26"', function() { expect(fb.fizzBuzz(26)).toEqual(26); });
	it('Should return "Fizz" when we are testing the number "32"', function() { expect(fb.fizzBuzz(32)).toEqual('Fizz'); });
	it('Should return "Buzz" when we are testing the number "52"', function() { expect(fb.fizzBuzz(52)).toEqual('Buzz'); });
});